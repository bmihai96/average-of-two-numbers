#We will create a function that uses 2 parameters and outputs the average of 2 numbers given as parameters
def compute_average(a,b):
    return (a+b)/2


if __name__ == '__main__':
    a=float(input("Give first number"))
    b=float(input("Give second number"))
    print("The average of the given numbers is :" + str(compute_average(a,b)))